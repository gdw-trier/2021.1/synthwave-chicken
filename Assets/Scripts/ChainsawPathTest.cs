using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct ChainsawSequenceBlueprint {
	public readonly Vector3[] points;
	public readonly Vector3[] rotations;
	public readonly float[] durations;
	public readonly int count;
	public readonly float total;

	public ChainsawSequenceBlueprint(Vector3[] points, Vector3[] rotations, float[] durations, int count) {
		this.points = points;
		this.rotations = rotations;
		this.durations = durations;
		this.count = count;
		total = 0;
		for (int i = 0; i < count; ++i) {
			total += durations[i];
		}
	}

	// #Performance
	public Sequence Create(Transform target) {
		target.localPosition = points[count - 1];
		target.localRotation = Quaternion.Euler(rotations[count - 1]);

		Sequence seq = DOTween.Sequence();
		seq.Append(target.DOLocalRotate(rotations[count - 1], 0));
		seq.SetAutoKill(false);
		seq.onComplete = () => { seq.Restart(); };

		for (int i = 0; i < count; ++i) {
			seq.Append(target.DOLocalMove(points[i], durations[i]).SetEase(Ease.Linear));
			seq.Append(target.DOLocalRotate(rotations[i], 0));
		}

		return seq;
	}

	public Sequence[] Fill(int amount, Transform parent, GameObject prefab) {
		Sequence[] seqs = new Sequence[amount];
		float toff = total / amount;
		for (int i = 0; i < amount; ++i) {
			var go = UnityEngine.Object.Instantiate(prefab, parent);
			seqs[i] = Create(go.transform);
			seqs[i].SetUpdate(UpdateType.Fixed);
			seqs[i].Goto(i * toff, true);
		}
		return seqs;
	}
}

public class ChainsawPathTest : MonoBehaviour {
	[SerializeField] private List<Transform> points;

	public float velocity = 1;

	public int amount;

	public Vector3[] normals;

	public bool roundDurations = true;

	public ChainsawSequenceBlueprint CreateBlueprint() {
		int count = points.Count;
		Vector3[] bakedPoints = new Vector3[count];
		Vector3[] rotations = new Vector3[count];
		normals = new Vector3[count];
		float[] durations = new float[count];

		for (int i = 0; i < count; ++i) {
			bakedPoints[i] = points[i].localPosition;
		}

		for (int i = 0; i < count; ++i) {
			var edge = bakedPoints[(i + 1) % count] - bakedPoints[i];
			var right = Vector3.Cross(edge, -bakedPoints[i]);
			var normal = Vector3.Cross(edge, right);
			normal.Normalize();
			rotations[i] = Quaternion.LookRotation(edge, normal).eulerAngles;
			normals[i] = normal;
		}

		for (int i = 1; i <= count; ++i) {
			float d = Vector3.Distance(bakedPoints[i % count], bakedPoints[i - 1]) / velocity;
			if (roundDurations) {
				d = Mathf.Floor(d / Time.fixedDeltaTime) * Time.fixedDeltaTime;
				d = Mathf.Max(d, Time.fixedDeltaTime);
			}
			durations[i % count] = d;

		}

		return new ChainsawSequenceBlueprint(bakedPoints, rotations, durations, count);
	}

	public Transform target;

	private Sequence[] sequences;

	private void Awake() {
		var bp = CreateBlueprint();
		sequences = bp.Fill(amount, transform, target.gameObject);
		foreach (var point in points) {
			Destroy(point.gameObject);
		}
		Destroy(target.gameObject);
	}

	private void OnDisable() {
		foreach (var seq in sequences) {
			seq.Kill();
		}
	}
}
