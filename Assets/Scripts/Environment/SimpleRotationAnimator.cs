using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotationAnimator : MonoBehaviour {
    [SerializeField] private float rotationSpeed = 3f;
    void Update(){
        this.transform.RotateAround(transform.position, transform.up, rotationSpeed * Time.deltaTime);
    }
}
