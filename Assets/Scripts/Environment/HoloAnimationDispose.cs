using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoloAnimationDispose : MonoBehaviour {
	public string field = "_Multiplicator";
	public float duration;
	public MeshRenderer target;

	private void Start() {
		target.material = new Material(target.material);
	}

	public void Play(float value) {
		DOTween.Sequence().Append(
			DOTween.To(
				() => target.material.GetFloat(field),
				(f) => target.material.SetFloat(field, f),
				value, duration
				));
	}
}
