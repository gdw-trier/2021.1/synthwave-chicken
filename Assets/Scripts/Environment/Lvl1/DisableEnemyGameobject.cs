using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableEnemyGameobject : MonoBehaviour
{
    public GameObject allEnemyGameobject;
    public void OnDisable()
    {
        allEnemyGameobject.SetActive(false);
    }
}
