using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {
	[SerializeField] private Transform left;
	[SerializeField] private Transform right;

	[SerializeField] private float openAmount;
	[SerializeField] private float transitionDuration;

	[SerializeField] private bool automatic;
	[ColorUsage(false, true)]
	[SerializeField] private Color automaticColor;
	[ColorUsage(false, true)]
	[SerializeField] private Color manualColor;

	[SerializeField] private Transform model;


	private bool isOpen = false;

	private SequenceQueue sequences = new SequenceQueue();

	private Vector3 leftOrg;
	private Vector3 rightOrg;

	private Material doorMaterialInstance;

	private void Awake() {
		leftOrg = left.localPosition;
		rightOrg = right.localPosition;

		var renderers = model.GetComponentsInChildren<MeshRenderer>();
		doorMaterialInstance = new Material(renderers[0].material);

		foreach (var renderer in renderers) {
			renderer.material = doorMaterialInstance;
		}

		SetAutomatic(automatic);
	}

	public void SetAutomatic(bool b) {
		automatic = b;
		doorMaterialInstance.SetColor("_EmissionColor", automatic ? automaticColor : manualColor);
	}

	public void Open() {
		if (isOpen) return;

		Sequence seqLeft = DOTween.Sequence();
		seqLeft.Append(left.DOLocalMove(leftOrg - Vector3.right * openAmount, transitionDuration));
		Sequence seqRight = DOTween.Sequence();
		seqRight.Append(right.DOLocalMove(rightOrg + Vector3.right * openAmount, transitionDuration));

		sequences.PushParallel(seqLeft, seqRight);

		//left.localPosition += Vector3.right * -openAmount;
		//right.localPosition += Vector3.right * openAmount;
		isOpen = true;
	}

	public void Close() {
		if (!isOpen) return;

		Sequence seqLeft = DOTween.Sequence();
		seqLeft.Append(left.DOLocalMove(leftOrg, transitionDuration));
		Sequence seqRight = DOTween.Sequence();
		seqRight.Append(right.DOLocalMove(rightOrg, transitionDuration));

		sequences.PushParallel(seqLeft, seqRight);

		//left.localPosition -= Vector3.right * -openAmount;
		//right.localPosition -= Vector3.right * openAmount;
		isOpen = false;
	}


	private void OnTriggerEnter(Collider other) {
		if (!automatic) return;
		if (other.tag == "Player") Open();
	}

	private void OnTriggerExit(Collider other) {
		if (!automatic) return;
		if (other.tag == "Player") Close(); 
	}

	private void OnDrawGizmos() {
		Gizmos.DrawIcon(transform.position + transform.TransformVector(Vector3.up), "Door Icon.png", true, automatic ? Color.green : Color.red);
	}
}
