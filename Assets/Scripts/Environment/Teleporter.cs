using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
	[SerializeField] private Transform target;

	private Transform player;

	private void Awake() {
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	public void Teleport() {
		player.position = target.position; ;
	}
}
