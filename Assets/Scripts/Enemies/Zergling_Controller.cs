using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
#if UNITY_EDITOR
using UnityEditor;
#endif


public class Zergling_Controller : MonoBehaviour
{

    public GameObject player;
    public GameObject hitParticle;
    public LayerMask layerMask;

    [Range(0, 2)]
    public float velocityAcceleration = 1f;
    public float slowDownDistance = 1f;
    [Range(0, 1)]
    public float slowDownAmount =0f;
    public float aggroRange = 10f;
    public bool isAggro;

    [Header("Knockback")]

    public float knockBackAmount;
    public float knockBackTimer;
    bool isKnockbacked;
    float currentKnockbackTimer;

    [Header("Attack")]

    public float attackNavMeshRad;
    public float attackDistance;
    public LayerMask attackLayerMask;
    private Vector3 rotVelocity;
    public float smoothRotValue;

    public int damageAmount;
    public float timeBetweenAttack;
    public float timeToApplyDamage;

    float currentTimeBetweenAttack;
    bool isAttacking;
    bool resetAttackTrigger;
    private Vector3 toPlayerDirection;
    Data.HealthComponent healthComponent;
    Data.HealthComponent playerHealthComponent;
    Rigidbody rb;

    Vector3 velocity = Vector3.zero;
    NavMeshAgent navMeshAgent;
    Animator anim;

    [Header("Patrol")]
    public bool canPatrol;
    public Transform[] patrolPoints;
    int currentPatrolPoint;
    [Header("RandomeHeightSpawn")]
    public bool hasRandomeHeight;
    public float minHeight;
    public float maxHeight;


    private void Awake()
    {
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        healthComponent = GetComponent<Data.HealthComponent>();   
    }

    // Start is called before the first frame update
    void Start()
    {
        if (hasRandomeHeight)
        {
            transform.localScale = new Vector3(Random.Range(minHeight,maxHeight), Random.Range(minHeight, maxHeight), Random.Range(minHeight, maxHeight));
        }

        navMeshAgent = GetComponent<NavMeshAgent>();
        currentKnockbackTimer = knockBackTimer;
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
        playerHealthComponent = player.GetComponent<Data.HealthComponent>();

        currentTimeBetweenAttack = timeBetweenAttack;
        currentPatrolPoint = 0;
    }

    private void OnEnable()
    {
        healthComponent.OnDamage += GotDamage;
    }

    private void OnDisable()
    {
        healthComponent.OnDamage -= GotDamage;
    }

    public void GotDamage(int amount, bool dead)
    {
        if (!isAggro)
        {
            isAggro = true;
        }
        GetKnockbacked(player);
        GameObject g = Instantiate(hitParticle, transform.position, Quaternion.identity);
        g.transform.SetParent(transform);
        Destroy(g, 1f);
        if (healthComponent.health <= 0)
        {
            GetComponent<Dismemberment>().DismemberWithForce(-toPlayerDirection.normalized);
            GameObject.Destroy(gameObject);
        }
    }

    public void GetKnockbacked(GameObject hitTarget)
    {
        isKnockbacked = true;
        rb.isKinematic = false;
        navMeshAgent.isStopped = true;
        Vector3 dir = -(hitTarget.transform.position - transform.position).normalized;
        rb.AddForce(dir * knockBackAmount,ForceMode.Impulse);
    }


    // Update is called once per frame
    void Update()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        if(isAggro)
        {
            if (!isKnockbacked)
            {
                toPlayerDirection = (player.transform.position - transform.position);
                Vector3 desVel = toPlayerDirection.normalized * velocityAcceleration;
                Vector3 steering = desVel - velocity;

                velocity += steering * Time.deltaTime;
                if (distanceToPlayer < slowDownDistance)
                {
                    velocity *= slowDownAmount;
                }

                navMeshAgent.SetDestination(transform.position + velocity);
            }
            else
            {
                if(currentKnockbackTimer <= 0 && !anim.GetBool("isDeactivated"))
                {
                    currentKnockbackTimer = knockBackTimer;
                    rb.isKinematic = true;
                    navMeshAgent.isStopped = false;
                    isKnockbacked = false;
                }
                else
                {
                    currentKnockbackTimer -= Time.deltaTime;
                }
            }
        }
        else
        {
            if(distanceToPlayer <= aggroRange && HasSightOnPlayer())
            {
                isAggro = true;
            }
            if (canPatrol)
            {
                velocity = Vector3.up;
                if (Vector3.Distance(transform.position,patrolPoints[currentPatrolPoint].position) < 2f)
                {
                    currentPatrolPoint++;
                    if(currentPatrolPoint >= patrolPoints.Length)
                    {
                        currentPatrolPoint = 0;
                    }
                }
                navMeshAgent.SetDestination(patrolPoints[currentPatrolPoint].position);
            }

        }

        if(distanceToPlayer <= attackDistance)
        {
            navMeshAgent.radius = attackNavMeshRad;
            Vector3 dirTarget = toPlayerDirection.normalized;
            dirTarget = new Vector3(dirTarget.x,0,dirTarget.z);
            transform.forward = Vector3.SmoothDamp(transform.forward, dirTarget, ref rotVelocity, smoothRotValue);
            
            RaycastHit hit;
            if (Physics.Raycast(transform.position,transform.forward,out hit ,attackDistance + 0.1f, attackLayerMask))
            {
                if (hit.collider.gameObject.CompareTag("Player") && !isAttacking)
                {
                    isAttacking = true;
                }
            }
            else
            {
                    isAttacking = false;
            }
        }
        else
        {
            navMeshAgent.radius = 1f;
        }

        anim.SetFloat("movementVelocity", velocity.magnitude);
        anim.SetFloat("inverseMovementVelocity", 1-velocity.magnitude);
        if (isAttacking) anim.SetTrigger("attack");
        if (isAttacking)
        {

            if(currentTimeBetweenAttack < 0)
            {
                isAttacking = false;
                currentTimeBetweenAttack = timeBetweenAttack;
            }
            else
            {
                currentTimeBetweenAttack -= Time.deltaTime;
            }
        }

        if (!isAttacking && resetAttackTrigger)
        {
            currentTimeBetweenAttack = timeBetweenAttack;
            resetAttackTrigger = false;
        }
        



    }

    bool HasSightOnPlayer()
    {
        Vector3 dir = (player.transform.position - transform.position).normalized;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, dir, out hit, 100, layerMask))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                return true;
            }
        }
        return false;
    }

    private void OnDrawGizmos()
    {
#if UNITY_EDITOR

        Handles.color = Color.red;
        Handles.DrawWireDisc(transform.position,Vector3.up, aggroRange);

        Handles.color = Color.blue;
        Handles.DrawWireDisc(transform.position, Vector3.up,attackDistance);
#endif
    }

    public void DealDamage()
    {
        playerHealthComponent.DealDamage(damageAmount);
    }

}
