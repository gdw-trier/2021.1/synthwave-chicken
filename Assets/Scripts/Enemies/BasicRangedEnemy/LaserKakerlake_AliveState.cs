using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LaserKakerlake_AliveState : IState
{
    LaserKakerLake_ScriptableObject enemyData;
    GameObject enemy;
    GameObject player;

    Rigidbody rb;
    NavMeshAgent navMeshAgent;
    Data.HealthComponent healthComponent;
    LayerMask layerMask;
    public bool isAggro;


    //States
    AI_StateMashine stateMashine;
    LaserKakerlake_RunTowardsState movementState;
    LaserKakerlake_AttackState attackState;
    LaserKakerlake_KnockbackState knockbackState;
    LaserKakerlake_RunAway runAwayState;
    LaserKakerlake_PatrolState patrolState;

    bool isDone;
    bool isRunning;

    public LaserKakerlake_AliveState(LaserKakerLake_ScriptableObject enemyData, GameObject enemy, GameObject player)
    {
        this.enemyData = enemyData;
        this.enemy = enemy;
        this.player = player;

        layerMask = enemy.GetComponent<LaserKakerlake_Controller>().layerMask;
        healthComponent = enemy.GetComponent<Data.HealthComponent>();

        stateMashine = new AI_StateMashine();
        movementState = new LaserKakerlake_RunTowardsState(enemyData, enemy, player);
        attackState = new LaserKakerlake_AttackState(enemyData, enemy, player);
        knockbackState = new LaserKakerlake_KnockbackState(enemyData, enemy, player);
        runAwayState = new LaserKakerlake_RunAway(enemyData, enemy, player);
        patrolState = new LaserKakerlake_PatrolState(enemy, enemyData);

        if (enemy.GetComponent<LaserKakerlake_Controller>().canPatrol)
        {
            stateMashine.ChangeState(patrolState);
        }
        else
        {
            stateMashine.ChangeState(movementState);
        }

    }

    public void Enter()
    {
        isDone = false;
        isRunning = true;
        isAggro = false;
    }


    public void Exit()
    {
        isRunning = false;
    }

    public void Run()
    {

        
        float distanceToPlayer = Vector3.Distance(enemy.transform.position, player.transform.position);


        if (isAggro)
        {


            if ((HasSightOnPlayer() && distanceToPlayer < enemyData.distanceBeforeAttack && distanceToPlayer > enemyData.whenToRunAwayDistance && movementState.StateIsRunning()) || patrolState.IsRunning())
            {
                stateMashine.ChangeState(attackState);
            }
            if (distanceToPlayer < enemyData.whenToRunAwayDistance && movementState.StateIsRunning())
            {
                stateMashine.ChangeState(runAwayState);
            }



            if (runAwayState.StateIsRunning() && runAwayState.StateIsDone())
            {
                stateMashine.ChangeState(movementState);
            }
            if (attackState.StateIsRunning() && attackState.StateIsDone())
            {
                stateMashine.ChangeState(movementState);
            }
            if (knockbackState.StateIsRunning() && knockbackState.StateIsDone())
            {
                stateMashine.ChangeState(movementState);
            }

            stateMashine.ExecuteState();
        }
        else
        {
            if(distanceToPlayer <= enemyData.aggroRange && HasSightOnPlayer())
            {
                isAggro = true;
            }
        }
        if (enemy.GetComponent<LaserKakerlake_Controller>().canPatrol && patrolState.IsRunning())
        {
            stateMashine.ExecuteState();
        }

    }

    public bool StateIsDone()
    {
        return isDone;
    }

    public bool StateIsRunning()
    {
        return isRunning;
    }

    public void OnDamage(int amount , bool dead)
    {
        if (!isAggro)
        {
            isAggro = true;
        }

        if(dead)
        {
            isDone = true;
        }
        else
        {
            stateMashine.ChangeState(knockbackState);
        }
    }


    bool HasSightOnPlayer()
    {
        Vector3 dir = (player.transform.position - enemy.transform.position).normalized;
        RaycastHit hit;
        if (Physics.Raycast(enemy.transform.position, dir, out hit, 100 , layerMask))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                return true;
            }
        }
        return false;
    }

}
