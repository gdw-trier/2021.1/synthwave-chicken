using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LaserKakerlake_DeathState : IState
{

    GameObject enemy;
    NavMeshAgent navMeshAgent;
    Rigidbody rb;
    ParticleSystem[] flameParticles;
    

    GameObject laserLeft;
    GameObject laserRight;
    GameObject laserParticleLeft;
    GameObject laserParticleRight;
    SomeInsaneLaserSoundControllerSaviourBullshitteryBlackMagic sukscsbb;

    float deadTimer = 1f;
    bool canDie;
    bool isRunning;

    public LaserKakerlake_DeathState(GameObject enemy)
    {
        this.enemy = enemy;
        navMeshAgent = enemy.GetComponent<NavMeshAgent>();
        rb = enemy.GetComponent<Rigidbody>();
        flameParticles = enemy.GetComponent<LaserKakerlake_Controller>().flameParticles;
        laserLeft = enemy.GetComponent<LaserKakerlake_Controller>().laserLeft;
        laserRight = enemy.GetComponent<LaserKakerlake_Controller>().laserRight;
        laserParticleLeft = enemy.GetComponent<LaserKakerlake_Controller>().laserLeftParticle;
        laserParticleRight = enemy.GetComponent<LaserKakerlake_Controller>().laserRightParticle;
        sukscsbb = enemy.GetComponent<SomeInsaneLaserSoundControllerSaviourBullshitteryBlackMagic>();
    }

    public void Enter()
    {
        navMeshAgent.enabled = false;
        rb.isKinematic = true;
        rb.isKinematic = false;
        rb.useGravity = true;
        sukscsbb.AbortChjager();

        foreach (ParticleSystem p in flameParticles)
        {
            p.transform.gameObject.SetActive(false);
        }
        laserLeft.SetActive(false);
        laserRight.SetActive(false);

        laserParticleLeft.SetActive(false);
        laserParticleRight.SetActive(false);
        float heightValue = Random.Range(5, 10f);
        rb.AddForce(Vector3.up * heightValue, ForceMode.Impulse);

        isRunning = true;

        MonoBehaviour.Destroy(enemy,5f);
    }

    public void Exit()
    {
        isRunning = false;
    }

    public void Run()
    {

        Vector3 randomeLeftDir= new Vector3(Random.Range(0, 1), Random.Range(0, 1), Random.Range(0, 1));
        enemy.transform.Rotate(Vector3.forward * 360f * Time.deltaTime);
        randomeLeftDir = new Vector3(Random.Range(0, 1), Random.Range(0, 1), Random.Range(0, 1));
        enemy.transform.Rotate(Vector3.right * 360f * Time.deltaTime);
        randomeLeftDir = new Vector3(Random.Range(0, 1), Random.Range(0, 1), Random.Range(0, 1));
        enemy.transform.Rotate(Vector3.up * 360f * Time.deltaTime);

        if (!canDie)
        {
            if (deadTimer < 0)
            {
                canDie = true;
            }
            else
            {
                deadTimer -= Time.deltaTime;
            }
        }
    }

    public bool GetCanDie()
    {
        return canDie;
    }

}
