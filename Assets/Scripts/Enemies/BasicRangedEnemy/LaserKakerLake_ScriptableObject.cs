using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LaserKakerlake", menuName = "ScriptableObjects/Enemy/LaserKakerlake", order =  6)]
public class LaserKakerLake_ScriptableObject : ScriptableObject
{
    public float movementSpeed =5f;
    public float movementFlameParticleEmissionRate;

    [Header("RunAway")]
    public float whenToRunAwayDistance = 5f;
    public float runAwayMovementSpeed = 10f;
    public float runAwayAccelerationSpeed = 20f;
    public float runAwayTime = 0.5f;


    [Header("KnockBack")]
    public float knockBackAmount = 40f;
    public float knockBackRotation = 20f;
    public float knockBackTimer = 0.2f;
    public float afterKnockBackTimer= 0.2f;


    [Header("Attack")]
    public float aggroRange = 15f; 
    public float distanceBeforeAttack = 10f;

    public int damageValue = 10;
    public float timeBeforeAttack = 2f;
    public float timeBeforeLaser = 1f;

    public float timeAfterAttackCooldown = 0.5f;

    public float laserStartWidth = 0f;
    public float laserWidthIncreasingRate = 0.07f;
    public float laserEndWidth = 0.3f;

    public float smoothRot = 1f;
    public float stopRotatingTime = 1.5f;
    public float attackFlameParticleEmissionRate;
}
