using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SomeInsaneLaserSoundControllerSaviourBullshitteryBlackMagic : MonoBehaviour
{
	private bool isChargin;
	private bool isLasering;

	[SerializeField] private AudioSource laser;
	[SerializeField] private AudioSource charge;

	public void BeginChargee() {
		if (isChargin) return;
		isChargin = true;
		charge.Play();
	}

	public void AbortChjager() {
		isChargin = false;
		isLasering = false;
		charge.Stop();
		laser.Stop();
	}

	public void FullLaser() {
		if (isLasering) return;
		isChargin = false;
		isLasering = true;
		laser.Play();
		charge.Stop();
	}

	public void EndLaser() {
		if (!isLasering) return;
		isLasering = false;
		laser.Stop();
	}
}
