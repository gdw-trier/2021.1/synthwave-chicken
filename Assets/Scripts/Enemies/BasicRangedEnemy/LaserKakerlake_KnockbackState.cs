using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



public class LaserKakerlake_KnockbackState : IState
{

    LaserKakerLake_ScriptableObject enemyData;
    GameObject enemy;
    GameObject player;
    GameObject laserLeft;
    GameObject laserRight;

    Rigidbody rb;
    NavMeshAgent navMeshAgent;

    float currentKnockBackTimer;
    float currentAfterKnockBackTimer;
    bool isKnockbacked;

    Vector3 startRot;
    float rotationAmount;

    bool isRunning;
    bool isDone;

    SomeInsaneLaserSoundControllerSaviourBullshitteryBlackMagic silscsbbm;

    public LaserKakerlake_KnockbackState(LaserKakerLake_ScriptableObject enemyData, GameObject enemy, GameObject player)
    {
        this.enemyData = enemyData;
        this.enemy = enemy;
        this.player = player;

        rb = enemy.GetComponent<Rigidbody>();
        navMeshAgent = enemy.GetComponent<NavMeshAgent>();
        laserLeft = enemy.GetComponent<LaserKakerlake_Controller>().laserLeft;
        laserRight = enemy.GetComponent<LaserKakerlake_Controller>().laserRight;
        silscsbbm = enemy.GetComponent<SomeInsaneLaserSoundControllerSaviourBullshitteryBlackMagic>();



    }

    public void Enter()
    {
        isRunning = true;
        isDone = false;
        isKnockbacked = true; ;

        laserLeft.SetActive(false);
        laserRight.SetActive(false);

        currentKnockBackTimer = enemyData.knockBackTimer;
        currentAfterKnockBackTimer = enemyData.afterKnockBackTimer;

        rb.isKinematic = false;
        navMeshAgent.isStopped = true;
        Vector3 dir = -(player.transform.position - enemy.transform.position).normalized;
        rb.AddForce(dir * enemyData.knockBackAmount, ForceMode.Impulse);

        silscsbbm.AbortChjager();
    }


    public void Run()
    {
        if (isKnockbacked)
        {
            enemy.transform.Rotate(enemy.transform.up * Time.deltaTime,enemyData.knockBackRotation);
            if (currentKnockBackTimer <= 0)
            {
                isKnockbacked = false;
                rb.isKinematic = true;
            }
            else
            {
                currentKnockBackTimer -= Time.deltaTime;
            }
        }

        else
        {
            enemy.transform.Rotate(enemy.transform.up * Time.deltaTime, enemyData.knockBackRotation / 2);
            if (currentAfterKnockBackTimer <= 0)
            {
                isDone = true;
            }
            else
            {
                currentAfterKnockBackTimer -= Time.deltaTime;
            }
        }

    }
    public void Exit()
    {
        navMeshAgent.isStopped = false;
        isRunning = false;
    }

    public bool StateIsDone()
    {
        return isDone;
    }

    public bool StateIsRunning()
    {
        return isRunning;
    }


}
