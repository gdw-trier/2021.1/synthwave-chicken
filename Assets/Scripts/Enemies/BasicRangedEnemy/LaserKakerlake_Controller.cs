using System.Collections;
using System.Collections.Generic;
using Enemies;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class LaserKakerlake_Controller : MonoBehaviour
{
    public LaserKakerLake_ScriptableObject enemyData;

    public GameObject player;

    public GameObject laserLeft;
    public GameObject laserLeftParticle;

    public GameObject laserRight;
    public GameObject laserRightParticle;

    public GameObject deathParticle;
    public ParticleSystem[] flameParticles;
    public GlobalEnemySettings GlobalEnemySettings;
    
    Data.HealthComponent healthComponent;

    public LayerMask layerMask;
    public LayerMask laserLayerMask;

    [Header("Patrollie")]
    public bool canPatrol;
    public Transform[] patrolPoints;
    
    AI_StateMashine stateMashine;
    LaserKakerlake_AliveState aliveState;
    LaserKakerlake_DeathState deathState;

    private DeactivatableAIComponent _deactivatableAIComponent;

    private void Awake()
    {
        if(player == null) {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        stateMashine = new AI_StateMashine();
        healthComponent = GetComponent<Data.HealthComponent>();
        aliveState = new LaserKakerlake_AliveState(enemyData, gameObject, player);
        deathState = new LaserKakerlake_DeathState(gameObject);

    }

    private void OnEnable()
    {
        healthComponent.OnDamage += aliveState.OnDamage; 
    }

    private void OnDisable()
    {
        healthComponent.OnDamage -= aliveState.OnDamage;
    }

    void Start()
    {
        stateMashine.ChangeState(aliveState);
        _deactivatableAIComponent = GetComponent<DeactivatableAIComponent>();
    }



    // Update is called once per frame
    void Update()
    {
        if(aliveState.StateIsRunning() && aliveState.StateIsDone())
        {
            stateMashine.ChangeState(deathState);
        }

        bool isActive = true;
        if (_deactivatableAIComponent != null)
        {
            isActive = _deactivatableAIComponent.settings.isAIActive;
        }
        
        if(isActive){
            stateMashine.ExecuteState();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (deathState.GetCanDie())
        {
            GameObject g = Instantiate(deathParticle, transform.position, Quaternion.identity);
            Destroy(g, 1f);
            GetComponent<Dismemberment>().DismemberWithForce(Vector3.up);

            Destroy(gameObject,0.1f);
        }
    }

    public void SetAggro(bool input)
    {
        aliveState.isAggro = input;
    }

    private void OnDrawGizmos()
    {
#if UNITY_EDITOR
        Handles.color = Color.yellow;
        Handles.DrawWireDisc(transform.position,Vector3.up, enemyData.distanceBeforeAttack);

        Handles.color = Color.blue;
        Handles.DrawWireDisc(transform.position,Vector3.up, enemyData.whenToRunAwayDistance);

        Handles.color = Color.red;
        Handles.DrawWireDisc(transform.position, Vector3.up, enemyData.aggroRange);
#endif
    }

}
