﻿using UnityEngine;

namespace Enemies
{
    [CreateAssetMenu(menuName = "ScriptableObjects/Global Enemy Settings", fileName = "GlobalEnemySettings")]
    public class GlobalEnemySettings : ScriptableObject
    {
        public bool isAIActive = true;
    }
}