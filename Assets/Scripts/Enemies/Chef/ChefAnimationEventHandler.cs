using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChefAnimationEventHandler : MonoBehaviour
{
    [SerializeField] private Zergling_Controller zerglingController;
    
    public void DamageEvent()
    {
        zerglingController.DealDamage();
    }
}
