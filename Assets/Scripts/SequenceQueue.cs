using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceQueue {
	private Sequence currentSequence;

	public void PushSequence(Sequence s) {
		if (currentSequence == null) {
			currentSequence = s;
			s.Play();
			return;
		}
		if (currentSequence.IsComplete() || !currentSequence.active) {
			currentSequence = s;
			s.Play();
			return;
		}

		s.Pause();
		currentSequence.onComplete += () => {
			s.Play();
		};
		currentSequence = s;
	}

	public void PushParallel(Sequence s, params Sequence[] seqs) {
		Sequence para = DOTween.Sequence();
		para.Append(s);

		s.Pause();
		para.Pause();

		foreach (var seq in seqs) {
			seq.Pause();
			para.Join(seq);
		}

		PushSequence(para);
	}

	public static SequenceQueue operator+(SequenceQueue queue, Sequence s) {
		queue.PushSequence(s);
		return queue;
	}
}
