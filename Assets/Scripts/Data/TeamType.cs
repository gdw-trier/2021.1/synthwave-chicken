﻿using System;
using UnityEngine;

namespace Data
{
    [Flags]
    public enum TeamType
    {
        Player = 1, Enemy = 2, Object = 4
    }
}