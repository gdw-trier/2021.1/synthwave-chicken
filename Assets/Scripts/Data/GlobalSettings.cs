﻿using UnityEngine;

namespace Data
{
    [CreateAssetMenu(menuName = "ScriptableObject/Settings/Globabl Settings", fileName = "GlobalSettings")]
    public class GlobalSettings : ScriptableObject
    {
        public bool motionBlur = true;
    }
}