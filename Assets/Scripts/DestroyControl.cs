using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyControl : MonoBehaviour
{
	public void DestroySelf(float t) {
		Destroy(gameObject, t);
	}
}
