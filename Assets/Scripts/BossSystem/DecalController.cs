using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DecalController : MonoBehaviour
{
    public float targetTime;
    public float startDistance;
    public GameObject horizontalLeft;
    public GameObject horizontalRight;
    public GameObject verticalLeft;
    public GameObject verticalRight;

    public void Action()
    {
        horizontalLeft.transform.position = transform.position + Vector3.left * startDistance;
        horizontalRight.transform.position = transform.position + Vector3.right * startDistance;
        verticalLeft.transform.position = transform.position + Vector3.forward * startDistance;
        verticalRight.transform.position = transform.position + Vector3.back * startDistance;

        horizontalLeft.transform.DOMove(transform.position, targetTime, false);
        horizontalRight.transform.DOMove(transform.position, targetTime, false);
        verticalLeft.transform.DOMove(transform.position, targetTime, false);
        verticalRight.transform.DOMove(transform.position, targetTime, false);
    }
}
