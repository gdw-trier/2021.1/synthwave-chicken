using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using DG.Tweening;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum BossAnimState
{
    Idling,
    Attacking,
    Walking,
    Charging
}

public class BossBehaviour : MonoBehaviour
{
    private StateMachine _stateMachine;
    private GameObject _player;
    private int _currentPatternState;
    private bool _rotating;
    private Quaternion _targetRotation;
    private HealthComponent _health;
    private Rigidbody _rigidbody;
    private BossAnimState _currentAnimState;
    private Renderer _renderer;
    private bool _flashing;
    public Animator animator;
    public BossSettings settings;
    public float rotateSpeed = 10f;
    public GameObject rangeAttackLaser;
    public GameObject stompingAttackBox;
    public GameObject model;
    public GameObject rig;
    public GameObject mobSpawnerPrefab;
    public GameObject trails;
    public GameObject mushroomPrefab;
    public Material flashMaterial;
    public PauseMenu pauseMenu;
    public GameObject bossLifeBar;

    public SoundMap growlMap;
    public AudioSource growlSource;

    private void OnDrawGizmos()
    {
        if(settings.showTargets)
        {
            Gizmos.color = Color.blue;
            GUI.color = Color.blue;
            for(int i = 0; i < settings.stompTargets.Count; i++)
            {
                Vector3 position = new Vector3(settings.stompTargets[i].x, transform.position.y, settings.stompTargets[i].y);
                Gizmos.DrawSphere(position, 0.1f);
#if UNITY_EDITOR
                Handles.Label(position + new Vector3(0, 0.5f, 0), "Target " + i.ToString());
#endif
            }
        }

        if(settings.showArea)
        {
            Gizmos.color = Color.green;
            //Gizmos.DrawWireCube(settings.areaCenter, settings.areaSize);
            Gizmos.DrawWireMesh(settings.dinoWalkArea.GetComponent<MeshFilter>().sharedMesh, Vector3.zero);
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(GameObject.FindGameObjectWithTag("Player").transform.position, settings.shootAreaSize);
            //Gizmos.DrawWireCube(settings.areaCenter, settings.shootAreaClamp);
        }
    }

    void Start()
    {
        _renderer = model.GetComponent<Renderer>();
        _rigidbody = GetComponent<Rigidbody>();
        _health = GetComponent<HealthComponent>();

        _health.OnDamage += FlashDamage;
        _health.OnDamage += DeathEvent;

        _player = GameObject.FindGameObjectWithTag("Player");
        _player.GetComponent<Rigidbody>().velocity = Vector3.zero;
        Physics.IgnoreCollision(_player.GetComponent<Collider>(), GetComponent<Collider>());
        _currentPatternState = 0;
        _stateMachine = new StateMachine();
        _stateMachine.InitStates(this);
        rangeAttackLaser.GetComponent<RangeAttackLaser>().SetDamagePerSecond(settings.damagePerSecondRange);
        stompingAttackBox.GetComponent<StompingAttackBox>().SetDamageToDeal(settings.hitDamage);
        stompingAttackBox.GetComponent<StompingAttackBox>().SetBoss(this);
    }

    void FixedUpdate()
    {
        if(!_rotating)
            _stateMachine.Execute();
        else if(!CheckRotationReached())
            RotateUpdate();
        else
        {
            _rotating = false;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.transform.name == "Walls" && _stateMachine.currentState == BossState.ChargeAtPlayer)
        {
            _rigidbody.velocity = Vector3.zero;
            _stateMachine.ChangeState(BossState.Idle);
        }
    }

    public void ChangeState(BossState state)
    {
        _stateMachine.ChangeState(state);
    }

    public void NextPatternState()
    {
        _stateMachine.ChangeState(settings.pattern[_currentPatternState]);
        _currentPatternState++;
        if(_currentPatternState >= settings.pattern.Count)
            _currentPatternState = 0;
    }

    public void LookInDirection(Vector3 direction)
    {
        Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
        /*
        Vector3 rotVector = rotation.eulerAngles;
        //rotVector.y -= 90;
        rotVector.x = 0;
        rotVector.z = 0;
        transform.rotation = Quaternion.Euler(rotVector);
        */
        transform.rotation = rotation;
    }

    public void RotateTowards(Vector3 direction)
    {
        Quaternion rotation = Quaternion.LookRotation(direction, Vector3.up);
        /*
        Vector3 rotVector = rotation.eulerAngles;
        //rotVector.y -= 90;
        rotVector.x = 0;
        rotVector.z = 0;
        _targetRotation = Quaternion.Euler(rotVector);
        */
        _targetRotation = rotation;
        _rotating = true;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    private void RotateUpdate()
    {
        //Debug.Log("Rotating");
        transform.rotation = Quaternion.Lerp(transform.rotation, _targetRotation, Time.fixedDeltaTime * rotateSpeed);
    }

    private bool CheckRotationReached()
    {
        float angle = Quaternion.Angle(transform.rotation, _targetRotation);
        return angle < 1f;
    }
    
    public Vector3 GetPlayerDirection()
    {
        Vector3 playerPos = _player.transform.position;
        playerPos.y = transform.position.y;

        Vector3 direction = (playerPos - transform.position).normalized;
        return direction;
    }

    public GameObject GetPlayer()
    {
        return _player;
    }

    public void EnableRangeLaser(bool b)
    {
        rangeAttackLaser.SetActive(b);
    }

    public void EnableStompBox(bool b)
    {
        stompingAttackBox.SetActive(b);
    }

    public BossState GetState()
    {
        return _stateMachine.currentState;
    }

    public void SpawnZerglings(int amount)
    {
        for(int i = 0; i < amount; i++)
        {
            /*
            float xPos = Random.Range(settings.areaCenter.x - settings.areaSize.x / 2,
                                    settings.areaCenter.x + settings.areaSize.x / 2);
            float zPos = Random.Range(settings.areaCenter.z - settings.areaSize.z / 2,
                                    settings.areaCenter.z + settings.areaSize.z / 2);
            Vector3 position = new Vector3(xPos, settings.cookSpawnHeight, zPos);
            */

            StartCoroutine(SpawnOneZerg(i));
        }
    }

    private IEnumerator SpawnOneZerg(float time)
    {
        yield return new WaitForSeconds(time * 0.2f);
        Vector3 position = GetRandomPointInMesh(settings.dinoWalkArea.GetComponent<MeshFilter>().sharedMesh);
        position.y = settings.cookSpawnHeight;

        GameObject spawner = Instantiate(mobSpawnerPrefab, position, Quaternion.identity);
        spawner.GetComponent<MobSpawner>().SetBoss(this);
    }

    public IEnumerator WaitAndActivateCollider()
    {
        yield return new WaitForSeconds(0.1f);
        GetComponent<Collider>().enabled = true;
    }

    public Vector3 GetRandomPointInMesh(Mesh m)
    {
        Vector3 point = SelectRandomMeshPoints.GetRandomPointInsideConvex(m);
        return point;
    }

    public void SetAnimState(BossAnimState state, bool active)
    {
        if(active)
            _currentAnimState = state;
        else
            _currentAnimState = BossAnimState.Idling;

        switch(state)
        {
            case BossAnimState.Idling:
                animator.SetBool("idling", active);
                break;
            case BossAnimState.Attacking:
                animator.SetBool("attacking", active);
                break;
            case BossAnimState.Walking:
                animator.SetBool("walking", active);
                break;
            case BossAnimState.Charging:
                animator.SetBool("charging", active);
                break;
        }
    }

    private void FlashDamage(int damage, bool died)
    {
        if(!_flashing)
            StartCoroutine(Flash());
    }

    private IEnumerator Flash()
    {
        _flashing = true;
        Material normal = _renderer.material;
        _renderer.material = flashMaterial;
        yield return new WaitForSeconds(0.1f);
        _flashing = false;
        _renderer.material = normal;
    }

    private void DeathEvent(int dmg, bool died)
    {
        if(died)
        {
            bossLifeBar.SetActive(false); 
            StartCoroutine(DeathAnim());
        }
    }

    private IEnumerator DeathAnim()
    {
        GetComponent<Collider>().enabled = false;
        for(int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        _stateMachine.Stop();
        GameObject mushroom = Instantiate(mushroomPrefab, transform.position, Quaternion.identity);
        mushroom.transform.DOScale(Vector3.one * 2, 2f);
        yield return new WaitForSeconds(3f);
        mushroom.transform.DOScale(Vector3.zero, 2f);
        Renderer[] children = mushroom.transform.GetComponentsInChildren<Renderer>();
        foreach(Renderer r in children)
        {
            r.material.DOFloat(0, "_Intensity", 1f);
        }
        yield return new WaitForSeconds(4f);
        pauseMenu.FinishLevel();
    }
}
