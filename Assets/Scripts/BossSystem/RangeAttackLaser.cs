using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;

public class RangeAttackLaser : MonoBehaviour
{
    public LayerMask mask;
    public GameObject particles;
    public GameObject model;
    private GameObject _player;
    private GameObject _boss;
    private HealthComponent _playerHealth;
    private bool _shouldDeal;
    public int _damagePerSecond;
    private float _accumulatedDamage;
    private Vector3 modelStartingPos;

    public float correctionAngle;

    private void Awake()
    {
        if(model != null) modelStartingPos = model.transform.position;
        _boss = transform.parent.gameObject;
        _player = GameObject.FindGameObjectWithTag("Player");
        _playerHealth = _player.GetComponent<HealthComponent>();
        _accumulatedDamage = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            _shouldDeal = true;
            _playerHealth.DealDamage(_damagePerSecond);
        }
    }

    private void Update()
    {
        if(model != null)
        {
            Vector3 rot = model.transform.rotation.eulerAngles;
            rot.x = 11f;
            model.transform.rotation = Quaternion.Euler(rot);

            Vector3 pos = model.transform.position;
            pos.y = modelStartingPos.y;
            model.transform.position = pos;
        }
    }

    private void FixedUpdate()
    {
        if(_shouldDeal)
        {
            _accumulatedDamage += Time.fixedDeltaTime * _damagePerSecond;
            if(_accumulatedDamage >= 1f)
            {
                _accumulatedDamage = 0;
                _playerHealth.DealDamage(_damagePerSecond);
            }
        }
        
        if(particles != null)
            MoveSparksAndScale();
    }

    private void OnTriggerExit(Collider other) {
        if(other.CompareTag("Player"))
        {
            _shouldDeal = false;
            _accumulatedDamage = 0;
        }
    }

    private void OnEnable()
    {
        if(particles != null)
            particles.SetActive(true);
    }

    private void OnDisable()
    {
        _shouldDeal = false;
        _accumulatedDamage = 0;
        if(particles != null)
            particles.SetActive(false);
    }

    public void SetDamagePerSecond(int damage)
    {
        _damagePerSecond = damage;
    }

    private void MoveSparksAndScale()
    {
        RaycastHit hit;
        //Vector3 direction = Quaternion.AngleAxis(-90, Vector3.up) * transform.forward;
        Vector3 startPos = _boss.transform.position;
        startPos.y = transform.position.y;
        //Debug.DrawLine(startPos, _boss.transform.right * 1000f, Color.blue, Time.fixedDeltaTime);
        if(Physics.Raycast(startPos, _boss.transform.forward, out hit, 1000f, mask))
        {
            //particles.transform.position = _player.transform.position + (hit.distance * transform.forward) - 0.1f * transform.forward;
            particles.transform.position = hit.point;
            //Debug.DrawLine(hit.point, hit.point + 10 * hit.normal, Color.cyan, Time.fixedDeltaTime);
            //particles.transform.rotation = Quaternion.Euler(hit.normal);
            Vector3 scale = transform.localScale;
            scale.y = hit.distance / 2;
            transform.localScale = scale;  
        }
    }
}
