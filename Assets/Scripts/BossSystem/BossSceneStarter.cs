using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSceneStarter : MonoBehaviour
{
    void Start()
    {
        GetComponent<Cutscene>().Play();
        Destroy(this);
    }
}
