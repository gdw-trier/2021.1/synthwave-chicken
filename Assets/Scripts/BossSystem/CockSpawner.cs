using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CockSpawner : MonoBehaviour
{
    public BossSettings settings;
    private BossBehaviour _boss;
    public GameObject cockPrefab;

    private void Start()
    {
        _boss = FindObjectOfType<BossBehaviour>();
        StartCoroutine(WaitAndSpawn());
    }

    private IEnumerator WaitAndSpawn()
    {
        yield return new WaitForSeconds(Random.Range(settings.minWaitTimeCocks, settings.maxWaitTimeCocks));
        GameObject cock = Instantiate(cockPrefab, transform.position, transform.rotation);
        cock.GetComponent<LaserKakerlake_Controller>().SetAggro(true);
        Physics.IgnoreCollision(cock.GetComponent<Collider>(), _boss.GetComponent<Collider>());
        StartCoroutine(WaitAndSpawn());
    }
}
