using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepareRangeState : IState
{
    private BossBehaviour _boss;
    private BossSettings _settings;
    private Vector3 _targetPos;
    private Vector3 _currentDirection;
    private Rigidbody _rigidbody;
    private float _distanceToWalk;

    public PrepareRangeState(BossBehaviour boss)
    {
        _boss = boss;
        _settings = boss.settings;
        _rigidbody = boss.GetComponent<Rigidbody>();
    }

    public void Enter()
    {
        //Debug.Log("Enter Prepare Range");
        GetNewPosition();
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        _boss.RotateTowards(_currentDirection);
        _boss.EnableStompBox(true);
        _boss.SetAnimState(BossAnimState.Walking, true);
    }

    public void Run()
    {
        if(!CheckIfWalkedDistance())
            MoveTowardsTarget();
        else
        {
            _boss.ChangeState(BossState.RangeAttack);
            _rigidbody.velocity = Vector3.zero;
        }
    }

    public void Exit()
    {
        _boss.EnableStompBox(false);
        _boss.SetAnimState(BossAnimState.Walking, false);
    }

    private void GetNewPosition()
    {
        /*
        float xPos = Random.Range(_settings.areaCenter.x - _settings.areaSize.x / 2, _settings.areaCenter.x + _settings.areaSize.x / 2);
        float zPos = Random.Range(_settings.areaCenter.z - _settings.areaSize.z / 2, _settings.areaCenter.z + _settings.areaSize.z / 2);
        Vector3 position = new Vector3(xPos, _boss.transform.position.y, zPos);
        */
        Vector3 position = _boss.GetRandomPointInMesh(_settings.dinoWalkArea.GetComponent<MeshFilter>().sharedMesh);
        position.y = _boss.transform.position.y;

        _targetPos = position;
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);
    }

    private void MoveTowardsTarget()
    {
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        Vector3 targetVelocity = _currentDirection * Time.fixedDeltaTime * _settings.stompSpeed;
        //float angle = Vector3.Angle(targetVelocity, _rigidbody.velocity);
        //if(_rigidbody.velocity.magnitude < 0.5f)
        _rigidbody.velocity = targetVelocity;

        _boss.LookInDirection(_currentDirection);
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);
    }

    private bool CheckIfWalkedDistance()
    {
        return _distanceToWalk < 2f;
    }
}
