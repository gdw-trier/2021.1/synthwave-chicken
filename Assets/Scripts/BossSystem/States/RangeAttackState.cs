using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeAttackState : IState
{
    private BossBehaviour _boss;
    private BossSettings _settings;
    private float _rotated;

    public RangeAttackState(BossBehaviour boss)
    {
        _boss = boss;
        _settings = boss.settings;
    }

    public void Enter()
    {
        _rotated = 0;
        Vector3 centerDirection = _settings.centerPoint - _boss.transform.position;
        centerDirection = centerDirection.normalized;

        centerDirection = Quaternion.AngleAxis(-_settings.rangeAttackDegrees / 2, Vector3.up) * centerDirection;

        _boss.RotateTowards(centerDirection);
        _boss.SetAnimState(BossAnimState.Attacking, true);
        _boss.growlMap.Play(_boss.growlSource);
    }

    public void Run()
    {
        if(_rotated < _settings.rangeAttackDegrees)
        {
            RotateUpdate();
            if(!_boss.rangeAttackLaser.activeSelf)
                _boss.EnableRangeLaser(true);
        }
        else
        {
            _boss.NextPatternState();
            _boss.EnableRangeLaser(false);
        }
    }

    public void Exit()
    {
        _boss.SetAnimState(BossAnimState.Attacking, false);
    }

    private void RotateUpdate()
    {
        float toRotate = Time.fixedDeltaTime * _settings.rotateSpeed;
        _rotated += toRotate;
        _boss.transform.rotation = Quaternion.Euler(_boss.transform.rotation.eulerAngles + new Vector3(0, toRotate, 0));
    }
}
