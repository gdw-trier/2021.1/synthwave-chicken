using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AfterChargeState : IState
{
    private BossBehaviour _boss;
    private BossSettings _settings;
    private Vector3 _currentDirection;
    private Vector3 _targetPos;
    private Rigidbody _rigidbody;
    private float _distanceToWalk;

    public AfterChargeState(BossBehaviour boss)
    {
        _boss = boss;
        _settings = boss.settings;
        _targetPos = boss.settings.centerPoint;
        _targetPos.y = boss.transform.position.y;
        _rigidbody = boss.GetComponent<Rigidbody>();
    }

    public void Enter()
    {
        Debug.Log("Enter after");
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        _boss.RotateTowards(_currentDirection);
        _boss.EnableStompBox(true);
        _boss.SetAnimState(BossAnimState.Walking, true);
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);
    }

    public void Run()
    {
        if(_boss.trails.activeSelf)
            _boss.trails.SetActive(false);
        if(!CheckIfWalkedDistance())
            MoveTowardsTarget();
        else
        {
            _boss.ChangeState(BossState.PrepareCharge);
            _rigidbody.velocity = Vector3.zero;
        }
    }

    public void Exit()
    {
        Debug.Log("exit after");
        _boss.EnableStompBox(false);
        _boss.SetAnimState(BossAnimState.Walking, false);
    }

    private void MoveTowardsTarget()
    {
        _currentDirection = (_targetPos - _boss.transform.position).normalized;
        Vector3 targetVelocity = _currentDirection * Time.fixedDeltaTime * _settings.stompSpeed;
        _rigidbody.velocity = targetVelocity;

        _boss.LookInDirection(_currentDirection);
        _distanceToWalk = Vector3.Distance(_boss.transform.position, _targetPos);
    }

    private bool CheckIfWalkedDistance()
    {
        return _distanceToWalk < 0.5f;
    }
}
