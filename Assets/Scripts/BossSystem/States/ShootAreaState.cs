using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootAreaState : IState
{
    private BossBehaviour _boss;
    private BossSettings _settings;
    private int _shootCount;
    private float _waitTime;
    private Transform _playerTransform;

    public ShootAreaState(BossBehaviour boss)
    {
        _boss = boss;
        _settings = boss.settings;
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    public void Enter()
    {
        _boss.growlMap.Play(_boss.growlSource);
        _shootCount = 0;
        SetNewWaitTime();
        _boss.SetAnimState(BossAnimState.Attacking, true);
    }

    public void Run()
    {
        if(_waitTime > 0)
        {
            _waitTime -= Time.fixedDeltaTime;
        }
        else
        {
            if(_shootCount < _settings.shootAmount)
            {
                SpawnAttack();
                SetNewWaitTime();
            }
            else
            {
                _boss.NextPatternState();
            }
        }
    }

    public void Exit()
    {
        _boss.SetAnimState(BossAnimState.Attacking, false);
    }

    private void SpawnAttack()
    {
        float xPos = Random.Range(_playerTransform.position.x - _settings.shootAreaSize.x / 2,
                                _playerTransform.position.x + _settings.shootAreaSize.x / 2);
        float zPos = Random.Range(_playerTransform.position.z - _settings.shootAreaSize.z / 2,
                                _playerTransform.position.z + _settings.shootAreaSize.z / 2);
        //xPos = Mathf.Clamp(xPos, -_settings.shootAreaClamp.x / 2, _settings.shootAreaClamp.x / 2);
        //zPos = Mathf.Clamp(zPos, -_settings.shootAreaClamp.z / 2, _settings.shootAreaClamp.z / 2);

        Vector3 position = new Vector3(xPos, 0, zPos);

        RaycastHit hit;
        if(!Physics.Raycast(new Vector3(xPos, -20f, zPos), Vector3.up, out hit, 20f, _settings.checkLaserMask))
        {
            float dist = Mathf.Sqrt(_settings.shootAreaSize.x * _settings.shootAreaSize.x + _settings.shootAreaSize.z * _settings.shootAreaSize.z);
            dist /= 2;
            Vector3 start = new Vector3(xPos, 0, zPos);
            Vector3 dir = (_settings.centerPoint - start).normalized;
            position += dir * dist;
        }

        if(Random.Range(0f,1f) < _settings.targetPlayerChance)
        {
            position.x = _playerTransform.position.x;
            position.z = _playerTransform.position.z;
        }

        GameObject missile = GameObject.Instantiate(_settings.areaAttackPrefab, position, Quaternion.identity);
        missile.GetComponent<AreaAttackMissile>().SetSettings(_settings);
        missile.GetComponent<AreaAttackMissile>().missile.GetComponent<RangeAttackLaser>().SetDamagePerSecond(_settings.damagePerSecondArea);
        missile.GetComponent<AreaAttackMissile>().StartAttack();
        _shootCount++;
    }

    private void SetNewWaitTime()
    {
        _waitTime = Random.Range(_settings.minWaitBetweenSpawn, _settings.maxWaitBetweenSpawn);
    }
}