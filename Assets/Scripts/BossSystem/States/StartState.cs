using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StartState : IState
{
    private BossBehaviour _boss;
    private BossSettings _setting;
    private float _waited;

    public StartState(BossBehaviour boss)
    {
        _boss = boss;
        _setting = boss.settings;
    }

    public void Enter()
    {
        //Debug.Log("Enter Start");
        _waited = 0;
        _boss.EnableStompBox(false);
    }

    public void Run()
    {
        if(_waited < _setting.waitToStart)
            _waited += Time.fixedDeltaTime;
        else
            _boss.NextPatternState();
    }

    public void Exit()
    {
    }
}
