using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepareChargeState : IState
{
    private BossBehaviour _boss;
    private BossSettings _settings;
    private float _waitTime;
    private float _timeToWait;

    public PrepareChargeState(BossBehaviour boss)
    {
        _boss = boss;
        _settings = boss.settings;
        _timeToWait = _settings.chargeWaitTime;
    }

    public void Enter()
    {
        //Debug.Log("Enter Prepare Charge");
        _waitTime = 0;
        _boss.RotateTowards(_boss.GetPlayerDirection());
        _boss.SetAnimState(BossAnimState.Idling, true);
    }

    public void Run()
    {
        _waitTime += Time.fixedDeltaTime;
        //Debug.Log("Wait Time: " + _waitTime + " / Time to Wait: " + _timeToWait);
        if(_waitTime > _timeToWait)
            _boss.ChangeState(BossState.ChargeAtPlayer);
        else
            _boss.LookInDirection(_boss.GetPlayerDirection());
    }

    public void Exit()
    {
        _boss.SetAnimState(BossAnimState.Idling, false);
    }
}

// Move to Center in Prepare!