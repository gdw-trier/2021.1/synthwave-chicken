using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;

public class StompingAttackBox : MonoBehaviour
{
    private HealthComponent _playerHealth;
    private ChickenController _chicken;
    private BossBehaviour _boss;
    private Rigidbody _bossRb;
    private int _damageToDeal;

    private void Start()
    {
        _chicken = GameObject.FindGameObjectWithTag("Player").GetComponent<ChickenController>();
        _playerHealth = _chicken.gameObject.GetComponent<HealthComponent>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && _bossRb.velocity.magnitude > 0)
        {
            //Debug.Log("BONK");
            _playerHealth.DealDamage(_damageToDeal);
            
            Vector3 playerDirection = _boss.GetPlayerDirection();
            playerDirection.y = _chicken.transform.position.y;
            _chicken.Knockback(playerDirection * _boss.settings.knockBackForce);

            if(_boss.GetState() == BossState.ChargeAtPlayer)
                _boss.NextPatternState();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player") && _bossRb.velocity.magnitude > 0 && _boss.GetState() != BossState.ChargeAtPlayer)
        {
            //_playerHealth.DealDamage(_damageToDeal);
            Vector3 playerDirection = _boss.GetPlayerDirection();
            playerDirection.y = _chicken.transform.position.y;
            _chicken.Knockback(playerDirection * _boss.settings.knockBackForce);
        }
    }

    public void SetDamageToDeal(int damage)
    {
        _damageToDeal = damage;
    }

    public void SetBoss(BossBehaviour boss)
    {
        _boss = boss;
        _bossRb = boss.GetComponent<Rigidbody>();
    }
}
