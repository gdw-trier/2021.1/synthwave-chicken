using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAnimatorStart : MonoBehaviour
{
    public float minWait, maxWait;

    private void Start() 
    {
        StartCoroutine(StartAnim());
    }

    private IEnumerator StartAnim()
    {
        yield return new WaitForSeconds(Random.Range(minWait, maxWait));
        GetComponent<Animator>().enabled = true;
        Destroy(this);
    }
}
