using Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunImpactHandler : MonoBehaviour {
	private ParticleSystem projectileEmitter;
	private List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>(16);
	private ParticleSystem.Particle[] particles;

	[SerializeField] private PlayerStats stats;

	[SerializeField] private Transform onHitEffect;
	[SerializeField] private float remainingLifetime = 0.01f;

	private int Damage => stats.gunDamage;
	[SerializeField] private TeamType teamTypeOrigin;
	private float Knockback => stats.gunKnockback;

	const int PARTICLE_BUFFER_SIZE = 10;

	private uint[] collidedParticles = new uint[PARTICLE_BUFFER_SIZE];
	private int cpfront = 0;


	[SerializeField] private SoundMap impact;

	private void Awake() {
		projectileEmitter = GetComponent<ParticleSystem>();
		particles = new ParticleSystem.Particle[projectileEmitter.main.maxParticles];
	}

	private void OnParticleCollision(GameObject other) {
		// VFX
		int numCollisionEvents = projectileEmitter.GetCollisionEvents(other, collisionEvents);

		if (numCollisionEvents == 0) return;

		int particleCount = projectileEmitter.GetParticles(particles);
		bool hit = false;

		for (int i = 0; i < numCollisionEvents; ++i) {
			var pos = collisionEvents[i].intersection;

			int closest = -1;
			float minDist = float.MaxValue;

			for (int p = 0; p < particleCount; ++p) {
				float dist = (pos - particles[p].position).sqrMagnitude;
				if (dist < minDist) {
					minDist = dist;
					closest = p;
				}
			}

			if (minDist >= 0.01f) {
				continue;
			}

			// Prevent multiple collisions with moving colliders by storing collided seeds
			uint seed = particles[closest].randomSeed;
			bool alreadyHandled = false;
			for (int x = 0; x < PARTICLE_BUFFER_SIZE; ++x) {
				if (collidedParticles[x] == seed) {
					alreadyHandled = true;
					break;
				}
			}

			if (alreadyHandled) continue;

			collidedParticles[cpfront] = seed;
			cpfront = (cpfront + 1) % PARTICLE_BUFFER_SIZE;

			particles[closest].remainingLifetime = remainingLifetime;

			hit = true;

			Transform effect = Instantiate(onHitEffect.gameObject).transform;
			impact.SpawnSource(pos);

			effect.position = pos;
			effect.rotation = Quaternion.LookRotation(collisionEvents[i].normal);

			break;
		}

		if (!hit) return;

		projectileEmitter.SetParticles(particles, particleCount);


		// Damage
		HealthComponent hc = other.GetComponent<HealthComponent>();
		if (hc != null && hc.DealDamage(Damage, ~teamTypeOrigin).Item1 && Knockback > 0) {
			hc.KnockBack(collisionEvents[0].intersection, Knockback);
		}
	}
}
