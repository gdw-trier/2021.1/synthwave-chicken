using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenAnimationEventHandler : MonoBehaviour {
	[SerializeField] private ChickenController c;


	public void AnimHandleShoot() {
		c.Shoot();
	}

	public void AnimHandleAttack1() {
		c.Melee(0);
	}

	public void AnimHandleAttack2() {
		c.Melee(1);
	}


}
