using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Position
{
    Left,
    Right
}

[CreateAssetMenu (fileName = "DialoguePart", menuName = "ScriptableObjects/Dialogue/DialoguePart")]
public class DialoguePart : ScriptableObject
{
    public GameObject actorLeft;
    public GameObject actorRight;
    public Position position;
    public string characterName;
    [TextArea(3,10)]
    public string[] sentences;
}
