using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public DialogueHolder dialogue;


    private void Awake()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")) { 
        
                DialogueSceneController.Instance.PlayDialogue(dialogue);
            
        }
    }
}
