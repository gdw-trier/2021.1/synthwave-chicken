using DG.Tweening;
using Enemies;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

public enum CutsceneSequenceType {
	/*
	 * Params: value, vector.x
	 * Optional value delay after fade
	 * Parallel if value == -1
	 * Duration = vector.x if vector.x > 0, GameObject configured value else
	 */
	FadeIn = 0,
	FadeOut = 1,

	/*
	 * Params: dialogue, vector.x, vector.y
	 * dialogue gets played
	 * vector x : delay before
	 * vector y : delay after
	 */
	Dialogue = 2,

	/*
	 * Params: QuestObjective
	 * QuestObjective gets progress
	 */
	Quest = 3,

	/*
	 * Params: event
	 * event gets invoked
	 */
	Event = 4,

	/*
	 * Params: value
	 * Value is used as duration of delay
	 */
	Delay = 5,

	MoveCamera = 6,

	/*
	 * Params: vector, transform, value
	 * Rotates chicken towards transform if set, otherwise vector is used as direction value
	 * Rotation is instant if value == 1
	 */
	ChickenLookAt = 7,

	/* Params: transform, value
	 * Chicken walks close to transform with value [0,1] percentage of speed
	 */
	ChickenMoveTo = 8,

	ChickenTeleport = 9,


	CutsceneUI = 20,
	GameUI = 21
}

[System.Serializable]
public struct CutsceneSequence {
	public CutsceneSequenceType type;
	public DialogueHolder dialogue;
	public QuestObejctive objective;
	public Transform transform;
	public float value;
	public Vector4 vector;
	public Camera cam;
	public UnityEvent @event;
}

public class Cutscene : MonoBehaviour {
	private SequenceQueue queue;
	public CutsceneSequence[] sequences;

	public GlobalEnemySettings enemySettings;

	public bool DisableControl = true;

	public float blendDuration = 0.4f;

	public float moveAccuracy = 0.25f;
	public float moveCloseEnough = 0.1f;

	private ChickenController chicken;

	public bool autoChangeUi = false;

	public UnityEvent OnCutsceneComplete;

	private void Start() {
		chicken = GameObject.FindGameObjectWithTag("Player").GetComponent<ChickenController>();
	}

	public void Play() {
		queue = new SequenceQueue();
		InputManager.Instance.EnableDialogueControl();
		CreateQueue();
	}

	private static Sequence StallingSequence(Action<Action> OnStart) {
		Sequence sequence = DOTween.Sequence();

		Action resume = () => {
			sequence.Goto(11, true);
		};

		sequence.AppendCallback(() => {
			OnStart(resume);
			sequence.Pause();
		});
		sequence.AppendInterval(10);

		return sequence;
	}

	private void CreateQueue() {
		if (autoChangeUi) CutsceneUI();
		queue += PrepareCutscene();
		foreach (var sub in sequences) {
			switch (sub.type) {
				case CutsceneSequenceType.Dialogue:
					if (sub.vector.x > 0) queue += DOTween.Sequence().AppendInterval(sub.vector.x);
					queue += DialogueSceneController.Instance.CreateDialogueSequence(sub.dialogue, false);
					if (sub.vector.y > 0) queue += DOTween.Sequence().AppendInterval(sub.vector.y);
					queue += PrepareCutscene();
					break;

				case CutsceneSequenceType.Quest:
					queue += DOTween.Sequence().AppendCallback(() => sub.objective.Progress());
					break;

				case CutsceneSequenceType.Event:
					queue += DOTween.Sequence().AppendCallback(() => sub.@event.Invoke());
					break;

				case CutsceneSequenceType.Delay:
					queue += DOTween.Sequence().AppendInterval(sub.value);
					break;

				case CutsceneSequenceType.FadeIn:
					queue += Fade(sub, true);
					break;

				case CutsceneSequenceType.FadeOut:
					queue += Fade(sub, false);
					break;

				case CutsceneSequenceType.MoveCamera:
					break;

				case CutsceneSequenceType.ChickenLookAt:
					queue += DOTween.Sequence().AppendCallback(() => ChickenLookAt(sub));
					break;

				case CutsceneSequenceType.ChickenMoveTo:
					queue += StallingSequence((resume) => StartCoroutine(MoveTo(sub, resume)));
					break;

				case CutsceneSequenceType.ChickenTeleport:
						queue += DOTween.Sequence().AppendCallback(() => chicken.transform.position = sub.transform.position);
						break;

				case CutsceneSequenceType.CutsceneUI:
					CutsceneUI();
					break;

				case CutsceneSequenceType.GameUI:
					GameUI();
					break;
			}
		}
		queue += CleanUpCutscene();
		if (autoChangeUi) GameUI();
		queue += DOTween.Sequence().AppendCallback(() => OnCutsceneComplete.Invoke());
	}

	private void CutsceneUI() {
		queue += DOTween.Sequence().AppendCallback(() => {
			DialogueSceneController.Instance.BlackBars.SetActive(true);
			DialogueSceneController.Instance.GameUI.SetActive(false);
			});
	}

	private void GameUI() {
		queue += DOTween.Sequence().AppendCallback(() => {
			DialogueSceneController.Instance.BlackBars.SetActive(false);
			DialogueSceneController.Instance.GameUI.SetActive(true);
		});
	}

	private Sequence Fade(in CutsceneSequence sub, bool fadeIn) {
		float duration = sub.vector.x > 0 ? sub.vector.x : blendDuration;
		Sequence seq = fadeIn ? BlackBlend.Instance.FadeIn(duration) : BlackBlend.Instance.FadeOut(duration);
		if (sub.value > 0) seq.AppendInterval(sub.value);
		else if (sub.value == -1) {
			seq.Pause();
			return DOTween.Sequence().AppendCallback(() => seq.Play());
		}
		return seq;
	}

	private void ChickenLookAt(in CutsceneSequence sub) {
		bool instant = sub.value == 1;
		if (sub.transform != null) {
			Vector3 vec = sub.transform.position - chicken.Model.transform.position;
			vec.y = 0;
			vec.Normalize();
			chicken.CutsceneOverrideViewDirection(vec, instant);
		}
		else {
			Vector3 vec = sub.vector;
			vec.y = 0;
			vec.Normalize();
			chicken.CutsceneOverrideViewDirection(vec, instant);
		}
	}

	private IEnumerator MoveTo(CutsceneSequence sub, Action resumeCutscene) {
		bool done = false;
		while (true) {
			Vector3 chickenPosition = chicken.Model.transform.position;
			chickenPosition.y = 0;

			Vector3 targetPosition = sub.transform.position;
			targetPosition.y = 0;

			// Distance Check
			float distance = Vector3.Distance(chickenPosition, targetPosition);

			if (done ||distance < moveCloseEnough) {
				if (!done) {
				chicken.CutsceneOverrideMoveVelocity(Vector2.zero);
				}
				done = true;

				if (!chicken.Moving) {
					resumeCutscene();
					yield break;
				}

				yield return null;
				continue;
			}

			// Rotate
			Vector3 direction = targetPosition - chickenPosition;
			direction.y = 0;
			direction.Normalize();
			chicken.CutsceneOverrideViewDirection(direction, false);

			// Move;
			float velocityFactor = Mathf.Clamp01(distance / moveAccuracy);

			Vector2 velocity = new Vector2(direction.x, direction.z);
			velocity *= (velocityFactor * sub.value);

			chicken.CutsceneOverrideMoveVelocity(velocity);
			yield return null;
		}
	}

	private Sequence PrepareCutscene() {
		return DOTween.Sequence().AppendCallback(() => {
			if (enemySettings != null) enemySettings.isAIActive = false;
			if (DisableControl) InputManager.Instance.EnableDialogueControl();
			DialogueSceneController.Instance.Camera.gameObject.SetActive(true);

		});
	}

	private Sequence CleanUpCutscene() {
		return DOTween.Sequence().AppendCallback(() => {
			if (enemySettings != null) enemySettings.isAIActive = true;
			if (DisableControl) InputManager.Instance.EnablePlayerControl();
			DialogueSceneController.Instance.Camera.gameObject.SetActive(false);
		});
	}
}


