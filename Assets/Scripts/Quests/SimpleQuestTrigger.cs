using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleQuestTrigger : MonoBehaviour {
	[SerializeField] private QuestObejctive quest;

	[SerializeField] private UnityEvent OnProgress;

	public void Trigger() {
			quest.Progress();
			OnProgress.Invoke();
			Destroy(gameObject);
	}

	private void OnTriggerEnter(Collider other) {
		if (other.tag == "Player" && !quest.Completed) {
			Trigger();
		}
	}
}
