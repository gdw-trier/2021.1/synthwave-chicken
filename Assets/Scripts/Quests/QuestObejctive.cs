using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Quest/Objective", fileName = "QuestObjective")]
public class QuestObejctive : ScriptableObject {
	public string ObjectiveText;
	public int RequiredCount = 1;
	public int CurrentCount;

	public bool Completed => CurrentCount >= RequiredCount;

	public delegate void QuestEventDelegate();

	public event QuestEventDelegate OnProgress;
	public event QuestEventDelegate OnComplete;

	public void Progress() {
		CurrentCount += 1;
		//Debug.Log($"Quest Progress: {name}");
		OnProgress?.Invoke();
		if (Completed) {
			OnComplete?.Invoke();
		}
	}
	private void Awake() {
		ResetProgress();
	}

	public void ResetProgress() {
		//Debug.Log($"Reset Progress: {name}");
		CurrentCount = 0;
	}
}
