using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectiveTrigger : MonoBehaviour {
	[SerializeField] public QuestObejctive objective;
	[SerializeField] public UnityEvent OnProgress;
	[SerializeField] public UnityEvent OnComplete;

	private void Start() {
		objective.OnProgress += HandleProgress;
		objective.OnComplete += HandleComplete;
	}

	private void OnDestroy() {
		objective.OnComplete -= HandleComplete;
		objective.OnProgress -= HandleProgress;
	}

	private void HandleProgress() {
		OnProgress.Invoke();
	}
	private void HandleComplete() {
		OnComplete.Invoke();
	}
}
