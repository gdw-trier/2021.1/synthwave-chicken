using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimplePlayerTrigger : MonoBehaviour {
	[SerializeField] private UnityEvent OnTrigger;

	[SerializeField] public bool triggerEnabled = true;

	public void SetTriggerEnabled(bool b) {
		triggerEnabled = b;
	}

	public void Trigger() {
		OnTrigger.Invoke();
	}

	private void OnTriggerEnter(Collider other) {
		if (!triggerEnabled) return;
		if (other.tag == "Player") {
			Trigger();
		}
	}

	public void Destroy(bool destroyGameObject) {
		if (destroyGameObject) Destroy(gameObject);
		else Destroy(this);
	}
}
