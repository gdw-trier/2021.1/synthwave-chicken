using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkSoundHandler : MonoBehaviour
{

	[SerializeField] private SoundMap walkingMetal;
	[SerializeField] private SoundMap walkingAsphalt;

	[SerializeField] private SoundSettings settings;

 	private AudioBuffer sources;
	private bool steppingLeft;
	private float LastStep;
	[SerializeField] private float stepTimeDelta;


	private void Awake() {
		sources = new AudioBuffer(2, gameObject);
	}

	private SoundMap map() {
		switch (settings.GroundType) {
			case GroundType.Steet:
				return walkingAsphalt;
			case GroundType.Metal:
				return walkingMetal;
			default:
				return null;
		}
	}


	public void StepLeft() {
		if (!steppingLeft || Time.timeSinceLevelLoad - LastStep > stepTimeDelta) {
			steppingLeft = true;
			LastStep = Time.timeSinceLevelLoad;
			map().Play(sources.Get());
		}
	}
	public void StepRight() {
		if (steppingLeft || Time.timeSinceLevelLoad - LastStep > stepTimeDelta) {
			steppingLeft = false;
			LastStep = Time.timeSinceLevelLoad;
			map().Play(sources.Get());
		}
	}
}
