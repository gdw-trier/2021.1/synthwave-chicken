using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectHelper : MonoBehaviour {
	public ParticleSystem p;
	public float destroyAfter = 0;

	private void OnEnable() {
		p.Play();
		if (destroyAfter > 0) {
			Destroy(gameObject, destroyAfter);
		}
	}

}
