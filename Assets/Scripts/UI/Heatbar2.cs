using Data.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class Heatbar2 : MonoBehaviour {
	[SerializeField] private Image[] bars;

	[SerializeField] private float blendTime;
	[SerializeField] private float colorBlendTime;

	[SerializeField] private PlayerHeat heat;

	[ColorUsage(true, hdr: true)]
	[SerializeField] private Color regular;

	[ColorUsage(true, hdr: true)]
	[SerializeField] private Color overheat;

	[SerializeField] private Image barPrefab;
	[SerializeField] private RectTransform barContainer;
	[SerializeField] private float barPadding;

	private float fill;
	private Color color;

	[SerializeField] private bool setEmissionColor;


	[ContextMenu("Create")]
	public void CreateBars() {
		float barHeight = (barContainer.rect.height - (bars.Length - 1) * barPadding) / bars.Length;
		float offset = -barContainer.rect.height / 2.0f;
		for (int i = 0; i < bars.Length; ++i) {
			if (bars[i] != null) DestroyImmediate(bars[i].gameObject);
			bars[i] = Instantiate(barPrefab.gameObject, barContainer).GetComponent<Image>();
			bars[i].rectTransform.anchorMin = new Vector2(0, 0.5f);
			bars[i].rectTransform.anchorMax = new Vector2(1, 0.5f);
			bars[i].rectTransform.pivot = new Vector2(0.5f, 0);
			bars[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, barHeight);
			bars[i].rectTransform.localPosition = new Vector3(0, offset + i * barHeight + i * barPadding, 0);
			bars[i].material = new Material(barPrefab.material);
#if UNITY_EDITOR
			EditorUtility.SetDirty(gameObject);
#endif
		}
	}


	[ContextMenu("Update Materials")]
	public void UpdateMaterials() {
		for (int i = 0; i < bars.Length; ++i) {
			if (bars[i] == null) continue;
			bars[i].material = new Material(barPrefab.material);
		}
#if UNITY_EDITOR
		EditorUtility.SetDirty(gameObject);
#endif
	}


	private void Awake() {
		UpdateMaterials();
		color = regular;
	}

	private void Update() {
		color = Color.Lerp(color, heat.overheated ? overheat : regular, colorBlendTime * Time.deltaTime);

		float desiredFill = (float)heat.heat / heat.maxHeat;

		fill = Mathf.Lerp(fill, desiredFill, blendTime * Time.deltaTime);

		float interval = 1.0f / bars.Length;
		int i = 0;

		for (float f = interval; f < fill && i < bars.Length; f += interval) {
			BlendBar(1, i);
			++i;
		}

		if (i >= bars.Length) return;

		BlendBar((fill - i * interval) / interval ,i);
		++i;
		
		for (; i < bars.Length; ++i) {
			BlendBar(0, i);
		}
	}

	private void BlendBar(float f, int i) {
		if (setEmissionColor) {
			bars[i].material.SetColor("_EmissionColor", color * f);
		}
		else {
			Color c = color;
			c.a = f;
			bars[i].material.color = c;
		}
	}
}
