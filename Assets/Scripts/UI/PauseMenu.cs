using Data;
using Enemies;
using UI;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour, PauseInput
{
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject deathMenu;
    [SerializeField] private GameObject finishMenu;
    [SerializeField] private PostProcessVolume postProcessVolume;
    [SerializeField] private PlaySettings playSettings;
    [SerializeField] private LevelCollection levelCollection;
    [SerializeField] private GlobalEnemySettings globalEnemySettings;
    private MotionBlur _motionBlur = null;

    private bool _isPaused;

    // Start is called before the first frame update
    void Start()
    {
        InputManager.Instance.pauseInput = this;
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        globalEnemySettings.isAIActive = true;
        if (postProcessVolume != null)
        {
            postProcessVolume.profile.TryGetSettings(out _motionBlur);
        }
    }

    public void TogglePause()
    {
        if (_isPaused)
        {
            ResumeGame();
        }
        else
        {
            PauseGame();
        }
    }

    public void PauseGame()
    {
        if (_isPaused) return;
        _isPaused = true;
        InputManager.Instance.DisablePlayerControl();
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        if (!_isPaused) return;
        globalEnemySettings.isAIActive = true;
        _isPaused = false;
        InputManager.Instance.EnablePlayerControl();
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void QuitMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    public void DeathScreen()
    {
        PauseGame();
        pauseMenu.SetActive(false);
        deathMenu.SetActive(true);
    }

    public void FinishLevel()
    {
        PauseGame();
        pauseMenu.SetActive(false);
        finishMenu.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Restart()
    {
        globalEnemySettings.isAIActive = true;
        SceneManager.LoadScene(playSettings.currentLevel.LevelNumber, LoadSceneMode.Single);
        CreditsPlayer.ResetCredits();
    }

    public void SetMotionBlur(Toggle toggle)
    {
        if (_motionBlur != null && toggle != null)
        {
            _motionBlur.active = toggle.isOn;
            Image background = toggle.GetComponentInChildren<Image>();
            if (background != null)
            {
                if (toggle.isOn)
                {
                    background.color = Color.green;
                }
                else
                {
                    background.color = Color.red;
                }
            }
        }
    }

    public void StartNextLevel()
    {
        globalEnemySettings.isAIActive = true;
        if (playSettings.isStoryPlaythrough)
        {
            int levelIndex = levelCollection.LevelPreviews.IndexOf(playSettings.currentLevel);
            if (levelIndex+1 >= levelCollection.LevelPreviews.Count)
            {
                playSettings.currentLevel = levelCollection.LevelPreviews[0];
            }
            else
            {
                playSettings.currentLevel = levelCollection.LevelPreviews[levelIndex+1];
                
            }
            SceneManager.LoadScene(playSettings.currentLevel.LevelNumber, LoadSceneMode.Single);
        }
        else
        {
            Restart();
        }
    }
}
