using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    [CreateAssetMenu (fileName = "levelCollection", menuName = "ScriptableObjects/Level Collection")]
    public class LevelCollection : ScriptableObject
    {
        [SerializeField]
        public List<LevelPreview> LevelPreviews = new List<LevelPreview>();
    }
}
