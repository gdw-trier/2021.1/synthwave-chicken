using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BlackBlend : MonoBehaviour
{
    public static BlackBlend Instance { get; private set; }

    private Image _image;

    private void Awake() {
        Instance = this;
    }

    private void Start()
    {
        _image = GetComponent<Image>();
    }

    public Sequence FadeOut(float duration)
    {
        return DOTween.Sequence().OnStart(() => _image.enabled = true).Append(_image.DOColor(Color.black, duration));
    }
    
    public Sequence FadeIn(float duration)
    {
        Color color = Color.black;
        color.a = 0;
        return DOTween.Sequence().Append(_image.DOColor(color, duration)).OnComplete(() => _image.enabled = false);
    }
}
