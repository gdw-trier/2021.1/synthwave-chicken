using Data;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private PlaySettings playSettings;
        
        public void StartGame()
        {
            playSettings.isStoryPlaythrough = true;
            playSettings.currentLevel = playSettings.allLevel.LevelPreviews[0];
            playSettings.currentTime = 0f;
            SceneManager.LoadScene(playSettings.currentLevel.LevelNumber, LoadSceneMode.Single);
            //SceneManager.LoadScene(1, LoadSceneMode.Additive);
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
