using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonHiglighter : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{

    
    [SerializeField] private Image[] additionalImages;
    private Image _image;
    private bool _isHighlighted = false;

    private void OnEnable()
    {
        _image = GetComponent<Image>();
        _image.material.DisableKeyword("_EMISSION");
        foreach (var image in additionalImages)
        {
            image.material.DisableKeyword("_EMISSION");
        }
    }

    private void OnDisable()
    {
        _image.material.DisableKeyword("_EMISSION");
        foreach (var image in additionalImages)
        {
            image.material.DisableKeyword("_EMISSION");
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        HighlightImage();
    }

    public void OnSelect(BaseEventData eventData)
    {
        HighlightImage();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        UnhighlightImage();
    }

    public void OnDeselect(BaseEventData eventData)
    {
        UnhighlightImage();
    }

    private void HighlightImage()
    {
        if (!_isHighlighted)
        {
            _image.material.EnableKeyword("_EMISSION");
            foreach (var image in additionalImages)
            {
                image.material.EnableKeyword("_EMISSION");
            }
            _isHighlighted = true;
        }
    }

    private void UnhighlightImage()
    {
        if (_isHighlighted)
        {
            _image.material.DisableKeyword("_EMISSION");
            foreach (var image in additionalImages)
            {
                image.material.DisableKeyword("_EMISSION");
            }
            _isHighlighted = false;
        }
    }
}
