using Data;
using Data.Player;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HeatBar : MonoBehaviour
    {
        [SerializeField] private PlayerHeat heat;
        [SerializeField] private Slider heatBar;
        [SerializeField] private Image fill;
        [SerializeField] private Color heatColor;
        [SerializeField] private Color overheatColor;
        [SerializeField] private float lerpBlendTime;

        private void Start()
        {
            heatBar.maxValue = heat.maxHeat;
            heatBar.value = heat.heat;
            fill.color = (heat.overheated) ? overheatColor : heatColor;
        }

        private void Update()
        {
            heatBar.maxValue = heat.maxHeat;
            heatBar.value = Mathf.Lerp(heatBar.value, heat.heat, Time.deltaTime * lerpBlendTime);
            fill.color = (heat.overheated) ? overheatColor : heatColor;
        }
    }
}
