using System;
using UnityEngine;

namespace UI
{
    [CreateAssetMenu (fileName = "levelData", menuName = "ScriptableObjects/Level Data")]
    public class LevelPreview : ScriptableObject
    {
        public int LevelNumber;
        public String Name;
        public Sprite Preview;
        public bool isSelectable = true;
        public bool isBossLevel = false;
    }
}
