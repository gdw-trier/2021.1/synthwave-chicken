using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class EditorWallPlacer : MonoBehaviour {
	public enum Direction {
		Up,
		Down,
		Left,
		Right,
		Forward,
		Back
	}

	public Vector3 offset;
	public int count;
	public Transform parent;

	public List<GameObject> clones = new List<GameObject>();

	public bool useRelative;
	public Direction relativeDirection;
	public float relativeOffset;



	[ExecuteInEditMode]
	public void Place(bool destroy) {
		if (destroy) {
			foreach (var go in clones) {
				DestroyImmediate(go);
			}
		}

		clones.Clear();
		clones.Capacity = count;

		GameObject prefab = Instantiate(gameObject);
		for (int i = 1; i <= count; ++i) {
			var go = Instantiate(prefab, parent);
			if (useRelative) {
				Vector3 dir;
				switch (relativeDirection) {
					case Direction.Up:
						dir = go.transform.up;
						break;
					case Direction.Down:
						dir = -go.transform.up;
						break;
					case Direction.Left:
						dir = -go.transform.right;
						break;
					case Direction.Right:
						dir = go.transform.right;
						break;
					case Direction.Forward:
						dir = go.transform.forward;
						break;
					case Direction.Back:
						dir = -go.transform.forward;
						break;
					default:
						dir = Vector3.zero;
						break;
				}
				dir *= i * relativeOffset;
				go.transform.position += dir;
			}
			else {
				go.transform.position = transform.position + i * offset;
			}
			clones.Add(go);
		}
		DestroyImmediate(prefab);
	}
}


#if UNITY_EDITOR
[CustomEditor(typeof(EditorWallPlacer))]
public class EditorWallPlacerEditor : Editor {
    SerializedProperty parts;

    public override void OnInspectorGUI() {
		DrawDefaultInspector();
		if (GUILayout.Button("Place")) {
			(target as EditorWallPlacer).Place(false);
		}
		if (GUILayout.Button("Replace")) {
			(target as EditorWallPlacer).Place(true);
		}
	}
}
#endif
