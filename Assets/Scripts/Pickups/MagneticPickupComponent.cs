using System;
using System.Collections;
using Data;
using Drops;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Pickups
{
    public class MagneticPickupComponent : MonoBehaviour
    {
        [SerializeField] public SphereCollider magneticCollider;
        [SerializeField] public Rigidbody rigidbody;
        [SerializeField] public HealthDropData data;

        private Transform _target;
        private bool _canPickUp = false;
        
        private void Start()
        {
            magneticCollider.radius = data.followStartRange;
            StartCoroutine(FollowTarget());
            StartCoroutine(PickupCooldown());
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_target == null && other.CompareTag("Player") && other.transform.GetComponent<HealthComponent>() != null)
            {
                _target = other.transform;
            }
        }

        IEnumerator FollowTarget()
        {
            while (true)
            {
                if (_target != null)
                {
                    rigidbody.AddForce((_target.transform.position - transform.position).normalized * (data.acceleration * Time.fixedDeltaTime));
                    rigidbody.velocity = Vector3.ClampMagnitude(rigidbody.velocity, data.maxSpeed);
                    
                    if (Vector3.Distance(transform.position, _target.transform.position) < data.pickUpRange)
                    {
                        if (_canPickUp)
                        {
                            _target.GetComponent<HealthComponent>()?.Heal(data.healValue);
                            Destroy(transform.parent.gameObject);
                            yield break;
                        }
                    }
                    
                }

                
                
                yield return new WaitForFixedUpdate();
            }
        }
        
        IEnumerator PickupCooldown()
        {
            yield return new WaitForSeconds(data.dropCooldown);
            _canPickUp = true;
        }
    }
}
