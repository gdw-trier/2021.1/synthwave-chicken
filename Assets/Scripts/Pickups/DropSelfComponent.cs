using System;
using Data;
using UnityEngine;

namespace Pickups
{
    public class DropSelfComponent : MonoBehaviour
    {
        [SerializeField] public HealthComponent healthComponent;
        [SerializeField] public float heightSpawn;
        [SerializeField] public bool canDropMultipleTimes = false;
        
        private bool _isDropped = false;
        private GameObject prefab;

        public void Start()
        {
            prefab = this.gameObject;
        }

        public void OnEnable()
        {
            healthComponent.OnDamage += OnDamage;
        }

        public void OnDisable()
        {
            healthComponent.OnDamage -= OnDamage;
        }

        public void OnDamage(int damage, bool died)
        {
            if (died && (!_isDropped || canDropMultipleTimes))
            {
                GameObject drop = Instantiate(prefab);
                drop.transform.position = new Vector3(transform.position.x, heightSpawn, transform.position.z);;
                _isDropped = true;
            }
        }
    }
}