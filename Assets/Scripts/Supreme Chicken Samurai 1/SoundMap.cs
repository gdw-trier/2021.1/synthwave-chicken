﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "SoundMap", menuName = "ScriptableObjects/SoundMap", order = 1)]
public class SoundMap : ScriptableObject {
    [System.Serializable]
    public struct Sound {
        public AudioClip clip;
    }
    [SerializeField] private float spatialBlend = 1;
    [SerializeField] private Sound[] sounds;
    [SerializeField] private AudioMixerGroup mixer;
    [SerializeField] private float pitch = 1;

    public float SpatialBlend => spatialBlend;

    public AudioMixerGroup outputAudioMixerGroup => mixer;

    public Sound Get() {
        return sounds[Random.Range(0, sounds.Length)];
    }

    public void SpawnSource(Transform origin) {
        SpawnSource(origin.position);
    }

    public void SpawnSource(Vector3 position) {
        var sound = Get();
        GameObject go = new GameObject();
        go.transform.position = position;
        var src = go.AddComponent<AudioSource>();
        src.spatialBlend = spatialBlend;
        src.clip = sound.clip;
        src.volume = 1;
        src.Play();
        Destroy(go, sound.clip.length + Time.fixedDeltaTime);
    }

    public void Play(AudioSource source, float offset = 0) {
        Sound s = Get();
        source.outputAudioMixerGroup = mixer;
        source.clip = s.clip;
        source.spatialBlend = spatialBlend;
        source.volume = 1;
        source.pitch = pitch;
        if (offset > 0) {
            source.PlayDelayed(offset);
        }
        else {
            source.Play();
        }
    }
}
