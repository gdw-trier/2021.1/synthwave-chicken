using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioBuffer {
	public GameObject gameObject;

	private AudioSource[] sources;

	private int nextSource;

	public AudioBuffer(int capacity, GameObject parent) {
		gameObject = parent;
		Allocate(capacity);
	}

	private void Allocate(int size) {
		sources = new AudioSource[size];
		for (int i = 0; i < size; ++i) {
			sources[i] = gameObject.AddComponent<AudioSource>();
		}
	}

	public AudioSource Get() {
		var source = sources[nextSource];
		nextSource++;
		nextSource %= sources.Length;
		return source;
	}

}
