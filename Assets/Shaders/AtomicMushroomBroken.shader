// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0,5,fgcg:0,5,fgcb:0,5,fgca:1,fgde:0,01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33703,y:32482,varname:node_3138,prsc:2|emission-2377-OUT,alpha-8559-OUT,clip-7254-OUT,voffset-4569-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32721,y:32436,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:0,7843137,c2:0,15908,c3:0,07843135,c4:1;n:type:ShaderForge.SFN_Time,id:3398,x:31463,y:32866,varname:node_3398,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6255,x:31674,y:32806,varname:node_6255,prsc:2|A-5178-OUT,B-3398-T;n:type:ShaderForge.SFN_ValueProperty,id:5178,x:31507,y:32762,ptovrint:False,ptlb:NoiseSpeed01,ptin:_NoiseSpeed01,varname:node_5178,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,1;n:type:ShaderForge.SFN_TexCoord,id:2948,x:31494,y:32414,varname:node_2948,prsc:2,uv:0,uaff:True;n:type:ShaderForge.SFN_ValueProperty,id:7979,x:31476,y:33102,ptovrint:False,ptlb:NoiseSpeed02,ptin:_NoiseSpeed02,varname:node_7979,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0,2;n:type:ShaderForge.SFN_Multiply,id:2897,x:31674,y:32995,varname:node_2897,prsc:2|A-3398-T,B-7979-OUT;n:type:ShaderForge.SFN_Posterize,id:7450,x:32603,y:32739,varname:node_7450,prsc:2|IN-2037-OUT,STPS-6410-OUT;n:type:ShaderForge.SFN_Tex2d,id:8447,x:31895,y:32436,ptovrint:False,ptlb:node_8447,ptin:_node_8447,varname:node_8447,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-2964-UVOUT;n:type:ShaderForge.SFN_Vector1,id:6410,x:32425,y:32903,varname:node_6410,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:2377,x:32887,y:32671,varname:node_2377,prsc:2|A-7241-RGB,B-9062-OUT;n:type:ShaderForge.SFN_Panner,id:2964,x:31927,y:32635,varname:node_2964,prsc:2,spu:0,spv:1|UVIN-2948-UVOUT,DIST-6255-OUT;n:type:ShaderForge.SFN_Panner,id:4202,x:31841,y:32933,varname:node_4202,prsc:2,spu:0,spv:1|UVIN-2948-UVOUT,DIST-2897-OUT;n:type:ShaderForge.SFN_Tex2d,id:8416,x:32006,y:32875,ptovrint:False,ptlb:node_8447_copy,ptin:_node_8447_copy,varname:_node_8447_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:0,isnm:False|UVIN-4202-UVOUT;n:type:ShaderForge.SFN_Multiply,id:9247,x:32198,y:32665,varname:node_9247,prsc:2|A-8447-RGB,B-8416-RGB;n:type:ShaderForge.SFN_Multiply,id:2037,x:32404,y:32739,varname:node_2037,prsc:2|A-9247-OUT,B-4182-OUT;n:type:ShaderForge.SFN_NormalVector,id:3395,x:33077,y:32418,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:7365,x:33251,y:32495,varname:node_7365,prsc:2|A-3395-OUT,B-7450-OUT;n:type:ShaderForge.SFN_ValueProperty,id:842,x:33251,y:32378,ptovrint:False,ptlb:VectorOffset,ptin:_VectorOffset,varname:node_842,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0,02;n:type:ShaderForge.SFN_Multiply,id:4569,x:33404,y:32471,varname:node_4569,prsc:2|A-842-OUT,B-7365-OUT;n:type:ShaderForge.SFN_Add,id:7025,x:33582,y:32510,varname:node_7025,prsc:2|A-8589-XYZ,B-4569-OUT;n:type:ShaderForge.SFN_ObjectPosition,id:8589,x:33470,y:32290,varname:node_8589,prsc:2;n:type:ShaderForge.SFN_Vector1,id:4216,x:32109,y:32330,varname:node_4216,prsc:2,v1:2;n:type:ShaderForge.SFN_TexCoord,id:5232,x:32887,y:32848,varname:node_5232,prsc:2,uv:0,uaff:True;n:type:ShaderForge.SFN_Power,id:7836,x:32330,y:32259,varname:node_7836,prsc:2|VAL-2948-V,EXP-4216-OUT;n:type:ShaderForge.SFN_Add,id:9062,x:32603,y:32558,varname:node_9062,prsc:2|A-7836-OUT,B-7450-OUT;n:type:ShaderForge.SFN_Power,id:8559,x:33211,y:32882,varname:node_8559,prsc:2|VAL-5232-V,EXP-5916-OUT;n:type:ShaderForge.SFN_Vector1,id:5916,x:33059,y:32951,varname:node_5916,prsc:2,v1:0,8;n:type:ShaderForge.SFN_ComponentMask,id:7254,x:33307,y:32712,varname:node_7254,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-2256-OUT;n:type:ShaderForge.SFN_Multiply,id:2256,x:32409,y:32988,varname:node_2256,prsc:2|A-9247-OUT,B-2817-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4182,x:32225,y:32835,ptovrint:False,ptlb:Density,ptin:_Density,varname:node_4182,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:7;n:type:ShaderForge.SFN_ValueProperty,id:2817,x:32225,y:33022,ptovrint:False,ptlb:ClipThreshold,ptin:_ClipThreshold,varname:node_2817,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:20;proporder:7241-5178-8447-7979-8416-842-4182-2817;pass:END;sub:END;*/

Shader "Shader Forge/AtomicMushroom" {
    Properties {
        [HDR]_Color ("Color", Color) = (0,7843137,0,15908,0,07843135,1)
        _NoiseSpeed01 ("NoiseSpeed01", Float ) = 0,1
        _node_8447 ("node_8447", 2D) = "white" {}
        _NoiseSpeed02 ("NoiseSpeed02", Float ) = -0,2
        _node_8447_copy ("node_8447_copy", 2D) = "white" {}
        _VectorOffset ("VectorOffset", Float ) = 0,02
        _Density ("Density", Float ) = 7
        _ClipThreshold ("ClipThreshold", Float ) = 20
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma target 3.0
            uniform sampler2D _node_8447; uniform float4 _node_8447_ST;
            uniform sampler2D _node_8447_copy; uniform float4 _node_8447_copy_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _Color)
                UNITY_DEFINE_INSTANCED_PROP( float, _NoiseSpeed01)
                UNITY_DEFINE_INSTANCED_PROP( float, _NoiseSpeed02)
                UNITY_DEFINE_INSTANCED_PROP( float, _VectorOffset)
                UNITY_DEFINE_INSTANCED_PROP( float, _Density)
                UNITY_DEFINE_INSTANCED_PROP( float, _ClipThreshold)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float _VectorOffset_var = UNITY_ACCESS_INSTANCED_PROP( Props, _VectorOffset );
                float _NoiseSpeed01_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed01 );
                float4 node_3398 = _Time;
                float2 node_2964 = (o.uv0+(_NoiseSpeed01_var*node_3398.g)*float2(0,1));
                float4 _node_8447_var = tex2Dlod(_node_8447,float4(TRANSFORM_TEX(node_2964, _node_8447),0.0,0));
                float _NoiseSpeed02_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed02 );
                float2 node_4202 = (o.uv0+(node_3398.g*_NoiseSpeed02_var)*float2(0,1));
                float4 _node_8447_copy_var = tex2Dlod(_node_8447_copy,float4(TRANSFORM_TEX(node_4202, _node_8447_copy),0.0,0));
                float3 node_9247 = (_node_8447_var.rgb*_node_8447_copy_var.rgb);
                float _Density_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Density );
                float node_6410 = 2,0;
                float3 node_7450 = floor((node_9247*_Density_var) * node_6410) / (node_6410 - 1);
                float3 node_4569 = (_VectorOffset_var*(v.normal*node_7450));
                v.vertex.xyz += node_4569;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float _NoiseSpeed01_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed01 );
                float4 node_3398 = _Time;
                float2 node_2964 = (i.uv0+(_NoiseSpeed01_var*node_3398.g)*float2(0,1));
                float4 _node_8447_var = tex2D(_node_8447,TRANSFORM_TEX(node_2964, _node_8447));
                float _NoiseSpeed02_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed02 );
                float2 node_4202 = (i.uv0+(node_3398.g*_NoiseSpeed02_var)*float2(0,1));
                float4 _node_8447_copy_var = tex2D(_node_8447_copy,TRANSFORM_TEX(node_4202, _node_8447_copy));
                float3 node_9247 = (_node_8447_var.rgb*_node_8447_copy_var.rgb);
                float _ClipThreshold_var = UNITY_ACCESS_INSTANCED_PROP( Props, _ClipThreshold );
                clip((node_9247*_ClipThreshold_var).r - 0.5);
////// Lighting:
////// Emissive:
                float4 _Color_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Color );
                float _Density_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Density );
                float node_6410 = 2,0;
                float3 node_7450 = floor((node_9247*_Density_var) * node_6410) / (node_6410 - 1);
                float3 emissive = (_Color_var.rgb*(pow(i.uv0.g,2,0)+node_7450));
                float3 finalColor = emissive;
                return fixed4(finalColor,pow(i.uv0.g,0,8));
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma target 3.0
            uniform sampler2D _node_8447; uniform float4 _node_8447_ST;
            uniform sampler2D _node_8447_copy; uniform float4 _node_8447_copy_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float, _NoiseSpeed01)
                UNITY_DEFINE_INSTANCED_PROP( float, _NoiseSpeed02)
                UNITY_DEFINE_INSTANCED_PROP( float, _VectorOffset)
                UNITY_DEFINE_INSTANCED_PROP( float, _Density)
                UNITY_DEFINE_INSTANCED_PROP( float, _ClipThreshold)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float _VectorOffset_var = UNITY_ACCESS_INSTANCED_PROP( Props, _VectorOffset );
                float _NoiseSpeed01_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed01 );
                float4 node_3398 = _Time;
                float2 node_2964 = (o.uv0+(_NoiseSpeed01_var*node_3398.g)*float2(0,1));
                float4 _node_8447_var = tex2Dlod(_node_8447,float4(TRANSFORM_TEX(node_2964, _node_8447),0.0,0));
                float _NoiseSpeed02_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed02 );
                float2 node_4202 = (o.uv0+(node_3398.g*_NoiseSpeed02_var)*float2(0,1));
                float4 _node_8447_copy_var = tex2Dlod(_node_8447_copy,float4(TRANSFORM_TEX(node_4202, _node_8447_copy),0.0,0));
                float3 node_9247 = (_node_8447_var.rgb*_node_8447_copy_var.rgb);
                float _Density_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Density );
                float node_6410 = 2,0;
                float3 node_7450 = floor((node_9247*_Density_var) * node_6410) / (node_6410 - 1);
                float3 node_4569 = (_VectorOffset_var*(v.normal*node_7450));
                v.vertex.xyz += node_4569;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float _NoiseSpeed01_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed01 );
                float4 node_3398 = _Time;
                float2 node_2964 = (i.uv0+(_NoiseSpeed01_var*node_3398.g)*float2(0,1));
                float4 _node_8447_var = tex2D(_node_8447,TRANSFORM_TEX(node_2964, _node_8447));
                float _NoiseSpeed02_var = UNITY_ACCESS_INSTANCED_PROP( Props, _NoiseSpeed02 );
                float2 node_4202 = (i.uv0+(node_3398.g*_NoiseSpeed02_var)*float2(0,1));
                float4 _node_8447_copy_var = tex2D(_node_8447_copy,TRANSFORM_TEX(node_4202, _node_8447_copy));
                float3 node_9247 = (_node_8447_var.rgb*_node_8447_copy_var.rgb);
                float _ClipThreshold_var = UNITY_ACCESS_INSTANCED_PROP( Props, _ClipThreshold );
                clip((node_9247*_ClipThreshold_var).r - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
